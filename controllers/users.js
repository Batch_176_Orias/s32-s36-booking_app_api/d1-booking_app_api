//section -> Dependencies and Module
	const User = require(`../models/User`); //model for user. Needed as the interface between app and db
	const Course = require(`../models/Course`);
	const bcrypt = require(`bcrypt`); //acquire the bcrypt library to perform hashing on the data
	const dotenv = require(`dotenv`); //integrate dotenv into the module
	const auth = require(`../auth`);

//section -> environment variable setup
	dotenv.config();
	const asin = Number(process.env.SALT);

//section -> functionalities [create]
	//register new account
	module.exports.register = (userData) => {
		let fName = userData.firstName;
		let lName = userData.lastName;
		let email = userData.email;
		let pass = userData.password;
		let mobil = userData.mobileNo;

		console.log(typeof(asin));

		let newUser = new User({
			firstName: fName,
			lastName: lName,
			email: email,
			password: bcrypt.hashSync(pass, asin),
			mobileNo: mobil
		});

		return newUser.save().then((success, fail)=>{
			if (success) {
				return success;
			} 
			else {
				return {message: `Error in registration`};
			}
		});
	}; 

//User Authentication
	/*
		steps:
		1. check db if user email exists
		2. compare the pw provided in login form w pass stored in db
		3. generate a json web token if user is successfully logged in, return false otherwise
	*/

	module.exports.loginUser = (data) => {
		//findOne method returns the first record in collection that matches the searched criteria
		return User.findOne({email: data.email}).then(result => {
			//user does not exist

			if(data == null){
				return false;
			}
			else{
				//user exists
				//compareSync is a method fr the bcrypt to be used in comparing the non encrypted pass fr login and the db pw. It return true or false depending on the result
				const isPasswordCorrect = bcrypt.compareSync(data.password, result.password);

				if(isPasswordCorrect){
					return { accessToken: auth.createToken(result.toObject()) };
				}
				else{
					//password not match
					return false;
				}
			}
		});
	};

	module.exports.getProfile = (data) => {
		return User.findById(data).then(result => {
			result.password = ``;
			return result;
		});
	};

//enroll registered users
	/*
		enrollment steps:
		1. look for the user by its ID
			-push the details of the course to be enrolled. Push the details to a new enrollment subdocument in the user.
		2. Look for the course by its ID
			-push the details of user whose enrolling. Push to a new enrollee subdocu in the course
		3. When both saving documents are successful, send a message to client. True if successful, false if not
	*/

	module.exports.enroll = async (req, res) => {
		console.log(`Test enroll route`);
		console.log(req.user.id); //user's id fr decoded token after verify
		console.log(req.body.courseId); //course ID fr request body

		if (req.user.isAdmin) {
			return res.send({message: `Authorization Forbidden`})
		} 

		//get user's ID to save courseId inside enrollments field

		let isUserUpdated = await User.findById(req.user.id).then((user) => {
			//add courseId in an object and push that object into user's enrollments array

			let newEnrollment = {
				courseId: req.body.courseId,
			};

			user.enrollments.push(newEnrollment);

			//save changes made to user docu
			return user.save().then((user)=>{
				return true;
			}).catch((error)=>{
				return error.message;
			});

			//if isUserUpdated does not contain the boolean true, stop the process and return a message to client
			if(isUserUpdated !== true){
				return res.send({message: isUserUpdated});
			}

		});

		//Find course id that will need to push enrollee
		let isCourseUpdated = await Course.findById(req.body.id).then((course) => {
			//create an obeject wc will be pushed to enrollee array/field
			let enrollee = {
				userId: req.user.id
			}

			course.enrollees.push(enrollee);

			//save course doucment
			return course.save().then((course)=>{
				return true;
			}).catch((error)=>{
				return error.message;
			});

			if(isCourseUpdated !== true){
				return res.send({message: isCourseUpdateds});
			};
		});

		//send message to the client that we have successfully enrolled our user if both isUserUpdated and isCourseUpdated contain the boolean true.

		if(isUserUpdated && isCourseUpdated) {
				return res.send({ message: "Enrolled Successfully" });
		};
	};

//section -> functionalities [retrieve]
//section -> functionalities [update]
//section -> functionalities [delete]