const Course = require(`../models/Course`);

/*create new course
	steps:
	1. Create new course object using the mongoose model and the information fr the request body
	2. Save new course to db
*/

module.exports.addCourse = (reqBody) => {
	//create a variable `newCourse` and instantiate the name, description, price

	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	});

	//save the created object to db
	return newCourse.save().then((sucess, fail) => {
		//course creation is successful or not
		if(fail){
			return false; 
		}
		else{
			return true;
		}
	}).catch(function(error){
		/*res.send(error);*/
		return error.message;

	});/*.catch(error=>{
		return error.message;
	});*/
};

module.exports.getAllCourses = (courses) => {
	return Course.find({}).then((success, fail)=>{
		if(success){
			return success;
		}
		else{
			return fail;
		}
	});
};

module.exports.getAllActive = (courses) => {
	return Course.find({ isActive: true }).then((success, fail) => {
		if (success) {
			return success;
		} 
		else {
			return fail;
		}
	}).catch(error => {
		return error.message;
	});
}

module.exports.getCourse = (courseId) => {
	return Course.findById(courseId).then((success, fail) => {
		if (success) {
			return success;
		} 
		else {
			return fail;
		}
	}).catch(error => {
		return error.message;
	});
};

/*
	update a course
		1. create var wc will contain the infor retrieved fr the req.body
		2. Find and update the course using the courseId retrieved fr the req.params and the variable containing info fr req.body
*/

	module.exports.updateCourse = (courseId, data) => {
		//specify the fields of the docu to be updated
		let newUpdatedCourse = {
			name: data.name,
			description: data.description,
			price: data.price
		}

		//findByIdAndUpdate
		return Course.findByIdAndUpdate(courseId, newUpdatedCourse).then((success, fail)=>{
			if (success) {
				return {message: `Course ${success.name} is updated`};
			} 
			else {
				return fail;
			}
		}).catch(error=>{
			return error.message;
		});
	};

//archiving a course
	//update status of isActive into false wc will no longer be displayed in client whenever all active courses are retrieved
	module.exports.archiveCourse = (updateId) => {
		let updateField = {
			isActive: false
		};

		return Course.findByIdAndUpdate(updateId, updateField).then((success, fail) => {
			if (success) {
				return {message: `record updated:`,
						result: success};
			} 
			else {
				return fail;
			}
		}).catch((error)=>{
			return error.message;
		});
	};