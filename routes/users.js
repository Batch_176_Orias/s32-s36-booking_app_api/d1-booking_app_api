//section - Dependencies and Module
	const express = require(`express`); //needed library to implement a new route
	const controller = require(`../controllers/users`);
	//controller component that holds all the business logic aspect of the app
	const auth = require(`../auth`);

//section - Routing Component
	//implement a new routing system dedicated for users collection
	const route = express.Router();

//section - Routes - POST
	route.post(`/register`, (req, res)=>{
		console.log(req.body);
		let userData = req.body;

		controller.register(userData).then(outcome=>{
			res.send(outcome);
		});
	});

//section - route for user authentication(login)

	route.post(`/login`, (req, res) => {
		controller.loginUser(req.body).then(result => {
			res.send(result);
		});
	});

//section - route for user authentication(login)
	//get user's details
	/*route.get(`/details`, (req, res) => {
		controller.getProfile(req.body).then(result => {
			res.send(result);
		});
	});*/

	route.get(`/details`, auth.verify, (req, res) => {
		controller.getProfile(req.user.id).then(result => {
			res.send(result);
		});
	});

//enroll registered users
	//only verified users can enroll in a course
	route.post(`/enroll`, auth.verify, controller.enroll);

//section - Routes - GET
//section - Routes - PUT
//section - Routes - DEL

//section - Expose Route System
	module.exports = route;