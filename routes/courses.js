const express = require(`express`);
const route = express.Router();
const courseConroller = require(`../controllers/courses`);
const auth = require(`../auth`);

//destructure the actual function that need to use

const { verify, verifyAdmin } = auth;

//Route for creating a course
	route.post(`/create`, verify, verifyAdmin, (req, res) => {
		console.log(req.user.isAdmin);
		courseConroller.addCourse(req.body).then(result => {
			res.send(result);
		});
	});

	route.get(`/all`, verify, verifyAdmin, (req, res) => {
		// res.send(`GET courses method is now up and running`);
		courseConroller.getAllCourses().then(result=>{
			res.send(result);
		});
	});

//get all active courses
	route.get(`/active`, (req, res) => {
		courseConroller.getAllActive().then(result => {
			res.send(result);
		});
	});

//retrieve specific course
	//req.params(short for parameter)
	route.get(`/:courseId`, (req, res) => {
		console.log(req.params.courseId);
		//retrieve the course ID by accessing the request's `params` property wc contains all parameters provided via url
		courseConroller.getCourse(req.params.courseId).then(result => {
			res.send(result);
		});
	});

//route for updating a course
	route.put(`/:courseId`, verify, verifyAdmin, (req, res) => {
		courseConroller.updateCourse(req.params.courseId, req.body).then(result => {
			res.send(result);
		});
	});

//archiving a course
	route.put(`/:courseId/archive`, verify, verifyAdmin, (req, res) => {
		courseConroller.archiveCourse(req.params.courseId).then((result) => {
			res.send(result);
		})
	});

module.exports = route;