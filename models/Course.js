//section - Dependencies and modules
	const mongoose = require(`mongoose`);

//section - Schema/Blueprint

	const courseSchema = mongoose.Schema({
		name: {
			type: String,
			required: [true, `Name is Required`]
		},
		description: {
			type: String,
			required: [true, `Description is Required`]
		},
		price: {
			type: Number,
			required: [true, `Course Price is Required`]
		},
		isActive: {
			type: Boolean,
			default: true
		}, 
		createdOn: {
			type: Date,
			default: new Date()
		}, 
		enrollees: [{

			userId: {
				type: String,
				required: [true, `Student ID is Required`]
			},
			enrolledOn: {
				type:Date,
				default: new Date()
			}
		}]
	});
//section - Model

	module.exports = mongoose.model(`Course`, courseSchema);