//Section - Modules and Dependencies
	const mongoose = require(`mongoose`);

//Section - Schema/Blueprint

	const userSchema = mongoose.Schema({
		firstName: {
			type: String,
			required: [true, `First name is Required`]
		},
		lastName: {
			type: String,
			required: [true, `Last name is Required`]
		},
		email: {
			type: String,
			required: [true, `Email is Required`]
		},
		password: {
			type: String,
			required: [true, `Password is Required`]
		},
		isAdmin: {
			type: Boolean,
			default: false
		},
		mobileNo: {
			type: String,
			required: [true, `Contact number is Required`]
		},
		enrollments: [
			{
				//every document that will be stored in the array structure would describe a course that the user is enrolled in
				courseId: {
					type: String,
					required: [true, `Subject ID is Required`]
				},
				enrolledOn: {
					type: Date,
					default: new Date()
				}, //date when the student enrolled
				status: {
					type: String,
					default: `Enrolled`
				} //status of enrolment
			}
		]
	});

//Section - Model

	module.exports = mongoose.model(`User`, userSchema);