//section dependencies and modules
	const express = require(`express`);
	const mongoose = require(`mongoose`);
	const dotenv = require(`dotenv`);
	const userRoutes = require(`./routes/users`); //routing system for users
	const courseRoutes = require(`./routes/courses`)

//section environment setup

	dotenv.config() //setup the environment

	let mongodb = process.env.CREDENTIALS;
	const port = process.env.PORT;

//section server setup
	const application = express();

	//app shoulc be able to recognize data written in json format
	application.use(express.json());

//section db connection
	mongoose.connect(mongodb);

	mongoose.connection.once(`open`, () => {
		console.log(`Database connected now`);
	})

//section backend routes
//http://localhost:4000/users
	application.use(`/users`, userRoutes); //separate all concerns/endpoint pertaining to diff collection in db
	//http://localhost:4000/courses
	application.use(`/courses`, courseRoutes);

//section server gateway response
	application.get(`/`, (req, res)=>{
		res.send(`Commencing Code Eater`);
	});

	application.listen(port, () => {
		console.log(`API is now hosted at port ${port}`);
	});